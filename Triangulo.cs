﻿using System;
using System.Globalization;

namespace Exercicio_Triangulo
{
    internal class Triangulo
    {
        public double A { get; set; }
        public double B { get; set; }  
        public double C { get; set; }

        public void EntradaDeDadosTrianguloX()
        {
            Console.WriteLine("Entre com as medidas do triângulo X: ");
            A = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            B = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            C = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
        }

        public void EntradaDeDadosTrianguloY()
        {
            Console.WriteLine("Entre com as medidas do triângulo Y: ");
            A = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            B = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
            C = double.Parse(Console.ReadLine(), CultureInfo.InvariantCulture);
        }

    }
}
